"use_strict";

const version_js = "0.0.4"
const version_link = 2

let status_div = document.getElementById("status");
status_div.innerHTML = "Getting Link info...";

async function doRedir(link_id, stay=false)
{
  //Try downloading ./links/<link_id>.json
  try{
    const response = await fetch(`links/${link_id}.json`);
    const redir_info = await response.json();
    const version = redir_info["version"];

    //--- Defaults ---
    // - V0
    let target_url  = null;
    // - V1
    let target_args = "";
    let track_url   = null;
    // - V2
    let inner_html  = null;

    //JSON has been parsed. Execute version-dependant code.
    //Execute highest version data first.

    //Version n...
    //if(redir_info["version"] >= n) { ... }

    //Version 2
    if(version >= 2){
      try   { inner_html = redir_info["inner_html"]; }
      catch { console.log(`[JS Redir] No inner HTML.`); }
      if (inner_html){
        status_div.innerHTML = atob(inner_html);
      }
    }

    //Version 1
    if(version >= 1){
      //keep_args
      if(redir_info["keep_args"]){
        try {
          let link = new URL(window.location.href);
          target_args = link.search;
        }
        catch {
          console.log(`[JS Redir] No link.`)
        }
      }
      //track
      try   { track_url = redir_info["track"]; }
      catch { console.log(`[JS Redir] No track URL.`); }
      if(track_url){
        try   { const response = await fetch(track_url); }
        catch { console.log(`[JS Redir] Track failed; Continuing.`); }
      }
    }

    //Version 0
    if(version >= 0){
      try   { target_url = redir_info["link"]; }
      catch { }
      if (!inner_html) {
        status_div.innerHTML = `Redirecting to: <a href="${target_url}${target_args}">${target_url}${target_args}</a>`;
      }
    }

    //Redirect user, unless we wanna stay
    if( (!stay) && (target_url) ) {
      window.location.replace(`${target_url}${target_args}`);
    }
  }
  catch (error){
    status_div.innerHTML = `Error! Invalid or deleted link.`;
  }
}
//Say hello!
console.log(`[JS Redir] Version: ${version_js}`);
console.log(`[JS Redir] Link version: ${version_link}`);

//Get the last element from the URL
let link = new URL(window.location.href);
let link_id = link.pathname.split("/").slice(-1)[0];
//If this link id is empty, try using the # part of the URL
if (link_id == ''){
  link_id = link.hash.replace("#", "");
}

//Some URLs are massive, especially if using the qr-artsy hack
//For these, the link ID is a hash of the current link id.
if (link_id.length > 80){
  var hash = sha1.create();
  hash.update(link_id);
  const hash_hex = hash.hex();
  link_id = `SHA1-${hash_hex}`;
}

let stay = link.searchParams.get("stay") === "true";
doRedir(link_id, stay);
