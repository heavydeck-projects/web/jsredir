# JSRedir

JSRedir is a link shortener service implementation that doesn't require any
server-side code, relying on static `.json` on the server and client's browser.

# Nginx configuration

An example configuration for using jsredir on nginx where the shortener URLs
lie under the `/s/<short link>` paths. Any and all links under this prefix will
serve the `index.html` file which will in turn extract the short link info
from the URL itself on the client.

```
location /s/ {
    root   /path/to/jsredir/root;
    index  index.html index.htm;
    try_files $uri /s/index.html;
}
```

# JSON specification

All attributes except `version` are optional.

## Version 0

| Key        | Type | Description                                            |
|------------|------|--------------------------------------------------------|
| `version`  | Int. | Must be at least `0` in value |
| `link`     | Str. | The target link the user will redirect to |

## Version 1

| Key             | Type | Description                                       |
|-----------------|------|---------------------------------------------------|
| `keep_args`     | Bool | If true, forward the short link GET arguments to the target link. |
| `track`         | Str. | If not empty, make a request to this url before redirecting. |

## Version 2

| Key             | Type | Description                                       |
|-----------------|------|---------------------------------------------------|
| `html`          | Str. | Base64-encoded HTML to be inserted on the redirect `<div>`. |